from pandas import *
import re
from os import listdir
from os.path import isfile, join
def load_data_and_concat(dir = '.', output_name = 'Origin'):
    onlyfiles = [f for f in listdir(dir) if isfile(join(dir, f))]
    print(onlyfiles)
    reg = re.compile("LoanStats.*\.csv")
    df_lst = []
    for i in onlyfiles:
        if re.match(reg,i):
            df_lst.append(read_csv(dir+"/"+i,skiprows=[0],skipfooter=2))
    result_df = concat(df_lst)
    result_df.to_pickle(dir+'/'+output_name+'.pkl')

# load_data_and_concat(dir = './testing',output_name='test')
